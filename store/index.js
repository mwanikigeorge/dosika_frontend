import Vuex  from "vuex";

const createStore = () => {
  return new Vuex.Store({
    state: () => ({
      winners: []
    }),
    mutations: {
      setwinners(state, winners){
        state.winners = winners
      }
    },
    actions:{
      nuxtServerInit(vuexContext, context) {
        // console.log('start fetching')
        return context.app.$axios.$post('/mshindi')
          .then(data => {
            // console.log(data.data)
            // const tipsArray = []

            // for (const key in data){
            //     tipsArray.push({...data[key]})
            // }
            vuexContext.commit('setwinners', data.data)
            // console.log('Finish fetching')
          })
          .catch(e => context.error(e))
      },

    	

    },
    getters: {
       
    }
  })
}

export default createStore